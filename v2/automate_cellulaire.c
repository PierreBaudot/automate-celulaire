#include "automate_cellulaire.h"

struct automate_cellulaire
{
	unsigned int taille;
	unsigned int num_generation;
	int *regle;
	int *cellules;
    int (*transition)(int*,int*);
};

ptr_ac creer_automate_cellulaire(int nb_etats, int* configuration_initiale,unsigned int taille, int* regle, int (*transition)(int*,int*)){
    ptr_ac a = (ptr_ac)malloc(sizeof(struct automate_cellulaire));
    a->taille = taille;
    a->num_generation = 0;
    a->regle = (int*)malloc(taille*sizeof(int));
    memcpy(a->regle,regle, taille*sizeof(int));
    a->transition = transition;

    if(configuration_initiale == NULL)
        a->cellules = (int *)calloc(taille,sizeof(int));
    else{
        for (unsigned int i =0; i<taille ; i++){
            if(configuration_initiale[i]<0 || configuration_initiale[i]>=nb_etats) return NULL;
        }
        a->cellules = (int *)malloc(taille*sizeof(int));
        memcpy(a->cellules, configuration_initiale, taille*sizeof(int));
    }

	return a;
}

void generation_suivante(ptr_ac a){
    a->num_generation++;
	int *next_gen = (int*)malloc(a->taille*sizeof(int));
	
	int* voisins  = (int*) malloc(TAILLE_VOISINAGE*sizeof(int));
	
	for(unsigned int i = 0 ; i < a->taille; i++){
		voisins[2] = a->cellules[(i == 0)? a->taille-1 : i-1];
		voisins[1] = a->cellules[i];
		voisins[0] = a->cellules[(i+1)%a->taille];
		
		next_gen[i] = a->transition(voisins,(a->regle));
	}
	free(a->cellules);
    a->cellules = next_gen;
	free(voisins);
}

void afficher_automate_cellulaire(const ptr_ac a,void (*afficher_cellules)(int*, unsigned int, unsigned int)){ //TODO
    (*afficher_cellules)(a->cellules,a->taille,a->num_generation);
}

void detruire_automate_cellulaire(ptr_ac a){
	free(a->regle);
	free(a->cellules);
	free(a);
}
#include "automate_cellulaire.h"
#include <stdio.h>

#define TAILLE_REGLE 10

void afficher_cellules_4_etats(int* tab, unsigned int taille, unsigned int iteration){
    printf("|%d\t|", iteration);
    for(unsigned int i = 0; i < taille; i++){
        switch (tab[i]){
            case 0:
                printf(" ");
                break;
            case 1:
                printf("X");
                break;
            case 2:
                printf("Y");
                break;
            case 3:
                printf("Z");
                break;
        }
    }
}

int transition_somme(int* voisnage, int* regle){
    return regle[voisnage[0]+voisnage[1]+voisnage[2]];
}

int main(int argc, char const *argv[])
{
    int const ITERATION_MAX = 16;
    int const LARG_MAX = 32;

    char* regle = (char *) malloc(sizeof(char));
    int ite=16,largeur=31;

    if (argc>3){
        printf("Erreur ! Trop d'arguments\nArguments attendus : Regle (en entier) et nombre d'Iterations\n");
        return 1;
    }else{
        if (argc>1) sscanf(argv[1],"%s",regle);
        else{
            printf("Regle : ");
            scanf("%s", regle);
        }
        if (argc>2)  sscanf(argv[2],"%d",&ite);
        else{
            do{
                printf("Iterations : ");
                scanf("%d", &ite);
                printf("%d",ite);
            }while(ite<0);
        }
        int res;
    }


    printf("\n");
    int* regle_convertie = (int*) malloc(TAILLE_REGLE* sizeof(int));
    for(int i = 0; i < TAILLE_REGLE; i++)
        regle_convertie[i] = regle[i] - '0';

    int tab[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    ptr_ac a = creer_automate_cellulaire(4,tab,largeur,regle_convertie,&transition_somme);
    free(regle_convertie);

    for(int i = 0; i < ite; i++){
        afficher_automate_cellulaire(a,&afficher_cellules_4_etats);
        printf("\n");
        generation_suivante(a);
    }
    detruire_automate_cellulaire(a);
    return 0;
}
#ifndef UTILS_H
#define UTILS_H

#include <stdlib.h>
#include <math.h>

typedef int* binary;

binary integer_to_binary(unsigned int i, int size); //Pierre
int binary_to_integer(binary i,int size);

#endif
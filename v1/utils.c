#include "utils.h"

binary integer_to_binary(unsigned int n,int size){
	binary res = (binary) malloc(size*sizeof(int));

	for(int j = 0; j < size; j++){
		res[j] = n%2;
		n /= 2;
	}

	return res;
}

int binary_to_integer(binary n,int size){
	int res = 0;

	for(int j = 0; j < size; j++)
		res += (n[j] == 0)? 0 : pow(2,j);

	return res;
}
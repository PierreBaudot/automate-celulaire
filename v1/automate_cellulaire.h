#ifndef AUTOMATE_CELLULAIRE_H
#define AUTOMATE_CELLULAIRE_H

#define TAILLE_REGLE 8
#define TAILLE_VOISINAGE 3

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "utils.h"

struct automate_cellulaire;

typedef struct automate_cellulaire* ptr_ac;

ptr_ac creer_automate_cellulaire(int* configuration_initiale,unsigned int taille, int regle); // Mathis

int evaluer_voisinage(binary voisinage, binary regle); // Pierre

void generation_suivante(ptr_ac automate_cellulaire); // Mathis

void afficher_automate_cellulaire(const ptr_ac a,void (*afficher_tableau)(int*, unsigned int, unsigned int)); // Pierre

void detruire_automate_cellulaire(ptr_ac automate_cellulaire); // Mathis

#endif

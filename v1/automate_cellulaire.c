#include "automate_cellulaire.h"

struct automate_cellulaire
{
	unsigned int taille;
	unsigned int num_generation;
	binary regle;
	int *cellules;
};

ptr_ac creer_automate_cellulaire(int* configuration_initiale,unsigned int taille, int regle){
	ptr_ac a = (ptr_ac)malloc(sizeof(struct automate_cellulaire));
    a->taille = taille;

	if(configuration_initiale == NULL)
        a->cellules = (binary)calloc(taille,sizeof(int));
	else{
        a->cellules = (binary)malloc(taille*sizeof(int));
        memcpy(a->cellules, configuration_initiale, taille*sizeof(int));
	}

    a->regle = integer_to_binary(regle, TAILLE_REGLE);
	return a;
}

void generation_suivante(ptr_ac a){
    a->num_generation++;
	int *next_gen = (int*)malloc(a->taille*sizeof(int));
	
	int* voisins  = (int*) malloc(TAILLE_VOISINAGE*sizeof(int));
	
	for(unsigned int i = 0 ; i < a->taille; i++){
		voisins[2] = a->cellules[(i == 0)? a->taille-1 : i-1];
		voisins[1] = a->cellules[i];
		voisins[0] = a->cellules[(i+1)%a->taille];
		
		next_gen[i] = evaluer_voisinage(voisins,(a->regle));
	}
	free(a->cellules);
    a->cellules = next_gen;
	free(voisins);
}

void detruire_automate_cellulaire(ptr_ac a){
	free(a->regle);
	free(a->cellules);
	free(a);
}

int evaluer_voisinage(binary voisinage, binary regle){
	int i = binary_to_integer(voisinage, TAILLE_VOISINAGE);
	return regle[i];
}

void afficher_automate_cellulaire(const ptr_ac a,void (*afficher_tableau)(int*, unsigned int, unsigned int)){
    (*afficher_tableau)(a->cellules,a->taille,a->num_generation);
}





















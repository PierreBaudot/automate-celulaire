#include "automate_cellulaire.h"
#include <stdio.h>

void afficher_tableau(int* tab, unsigned int taille, unsigned int iteration){
    printf("|%d\t|", iteration);
    for(unsigned int i = 0; i < taille; i++)
        printf("%c ", (tab[i]==0)?' ':'X');
}

int main(int argc, char const *argv[])
{
	int const ITERATION_MAX = 16;
	int const LARG_MAX = 32;

	int* read = malloc(sizeof(int));
	int regle=30,ite=16,largeur=31;

	if (argc>3){
        printf("Erreur ! Trop d'arguments\nArguments attendus : Regle (en entier) et nombre d'Iterations\n");
	    return 1;
	}else{
	    if (argc>1) sscanf(argv[1],"%d",&regle);
	    else{
            do{
                printf("Regle (0-255) : ");
                scanf("%d", read);
                regle=*read;
            }while(regle<0 || regle>255);
	    }
        if (argc>2)  sscanf(argv[2],"%d",&ite);
        else{
            do{
                printf("Iterations : ");
                scanf("%d", read);
                ite=*read;
            }while(ite<0);
        }
        int res;
	}
	
	
	printf("\n");
	
	
	int tab[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	ptr_ac a = creer_automate_cellulaire(tab,largeur,regle);
	for(int i = 0; i < ite; i++){
		afficher_automate_cellulaire(a,&afficher_tableau);
		printf("\n");
		generation_suivante(a);
	}
	detruire_automate_cellulaire(a);
	return 0;
}


#ifndef AUTOMATE_CELLULAIRE_H
#define AUTOMATE_CELLULAIRE_H

/** nombre de cellules qui composent un voisinage dans un automate a une dimension */
#define TAILLE_VOISINAGE 3

#include <math.h>
#include "image_pgm.h"

struct automate_cellulaire;

/** raccourci pour creer un pointeur sur un struct automate_cellulaire*/
typedef struct automate_cellulaire* ptr_ac;

ptr_ac creer_automate_cellulaire(int nb_etats, int* configuration_initiale,int nb_cellules,
        int* regle, int taille_regle, int (*transition)(int*, int),
        int nb_generation);

ptr_ac creer_automate_cellulaire_depuis_fichier(const char* chemin, int (*transition)(int*, int), int nb_generations);

void afficher_automate_cellulaire(const ptr_ac a,void (*afficher_cellules)(int**, int, int, int));

void detruire_automate_cellulaire(ptr_ac a);


////////////////////////////////////////// FONCTIONS DE TRANSITION PREDEFINIES //////////////////////////////////////////

int transition_somme(int* voisnage, int taille_regle);

int transition_binaire(int* voisnage, int taille_regle);


/////////////////////////////////////////// FONCTIONS D'AFFICHAGE PREDEFINIES ///////////////////////////////////////////

void affichage_image_automate(int** cellules, int nb_cellules, int nb_generations, int nb_etats);

void affichage_console_automate_4_etats_max(int** cellules, int nb_cellules, int nb_generations, int nb_etats);


#endif

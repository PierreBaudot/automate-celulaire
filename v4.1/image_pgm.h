#ifndef IMAGE_H
#define IMAGE_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

/** Valeur maximale que peut prendre un pixel d'une image PGM. */
#define VALEUR_MAX_PIXEL 65536

struct image_pgm;

/** Représente le type d'encodage d'une image PGM. */ // cette enum est déclarée ici pour pouvoir acceder a ses valeurs en dehors du .c
typedef enum { P2 /*!< image en niveaux de gris codée en ASCII*/
               ,P5 /*!< image en niveaux de gris codée en binaire*/
} format_pgm;

/** raccourci pour creer un pointeur sur un struct image_pgm*/
typedef struct image_pgm* ptr_img;

ptr_img lire_image_pgm(char* path);

ptr_img creer_image_pgm_noire(unsigned int largeur, unsigned int hauteur, unsigned int valeur_max, format_pgm nb_magique);
ptr_img tableau_vers_image_pgm(unsigned int** tab, unsigned int largeur, unsigned int hauteur, unsigned int valeur_max, format_pgm nb_magique);
ptr_img copier_image_pgm(const ptr_img img);

unsigned int get_largeur_image_pgm(ptr_img img);
unsigned int get_hauteur_image_pgm(ptr_img img);
unsigned int get_valeur_max_image_pgm(ptr_img img);
unsigned int get_pixel_image_pgm(unsigned int x, unsigned int y, ptr_img img);
void set_pixel_image_pgm(unsigned int x, unsigned  int y, unsigned int valeur, ptr_img img);

bool enregistrer_image_pgm(ptr_img pgm, char* chemin);

void detruire_image_pgm(ptr_img img);

bool filtrer_image_pgm(ptr_img img, void(*filtre)(struct image_pgm img), char* image_sortie);

/////////// FILRES ///////////

void filtre_negatif(struct image_pgm i);

#endif
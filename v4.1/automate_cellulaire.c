#include "automate_cellulaire.h"

/**
 * @struct automate_cellulaire
 * @brief représente un automate cellulaire a une dimention
 */
struct automate_cellulaire
{
    int nb_cellules; /*!< Nombre de cellules dans la ligne (largeur de la matrice cellules). */
    int nb_generation; /*!< Nombre de générations a calculer (hauteur de la matrice cellules). */
    int nb_etats; /*!< Nombre d'états que peut prendre une cellule. */
    int *regle; /*!< Tableau d'entiers permettant de calculer l'état suivant d'une cellule en fonction de son voisinage.*/
    int taille_regle; /*!< Taille du tableau contenant la règle */
    int **cellules; /*!< Stocke la configuration initiale (de taille nb_cellules) ainsi que les nb_generation-1 générations suivantes.*/
    int (*transition)(int*, int); /*!< Fonction permettant de calculer l'etat d'une cellule à la génération suivante qui
                                    * prend en paramètre dans l'ordre :
                                    *
                                    *   1) Un tableau de taille TAILLE_VOISINAGE contenant l'état des cellules (dans l'ordre
                                    *   d'apparition dans la ligne) du voisinage de la cellule dont ont veut calculer l'état
                                    *   a la génération suivante.
                                    *
                                    *   2) la taille de la règle.
                                    *
                                    *   Cette fonction doit retourner l'indice ou aller chercher l'état suivant dans la règle.
                                    *   @see TAILLE_VOISINAGE*/
};


/**
 * Calcule la generation suivante d'un automate cellulaire.
 * @param a automate sur lequel passer a la generation suivante.
 * @return true si l'operations s'est bien passé, false sinon.
 */
bool generation_suivante(ptr_ac a){
    a->cellules[a->nb_generation] = (int*)malloc(a->nb_cellules*sizeof(int));
    if(a->cellules[a->nb_generation] == NULL) return false;

    a->nb_generation++;

    int* voisins  = (int*) malloc(TAILLE_VOISINAGE*sizeof(int));
    if(voisins == NULL) return false;

    int indice_regle;
    for(int i = 0 ; i < a->nb_cellules; i++){
        voisins[2] = a->cellules[a->nb_generation-2][(i == 0)? a->nb_cellules-1 : i-1];
        voisins[1] = a->cellules[a->nb_generation-2][i];
        voisins[0] = a->cellules[a->nb_generation-2][(i+1)%a->nb_cellules];

        indice_regle = a->transition(voisins,a->taille_regle);
        if(indice_regle < 0 || indice_regle >= a->taille_regle){
            free(voisins);
            fprintf(stderr, "L'indice retourné par la fonction de transition est en dehors de la règle : %d\n", indice_regle);
            return false;
        }
        a->cellules[a->nb_generation-1][i] = a->regle[indice_regle];
    }
    free(voisins);
    return true;
}

/**
 * Crée un nouvel automate celulaire.
 *
 * @param nb_etats Nombre d'états que peut prendre une cellule.
 * @param configuration_initiale Tableau d'entiers (compris entre 0 et nb_etat-1 inclus) de la premiere génération de
 * cellules (ce tableau sera copié).
 * @param nb_cellules Taille du tableau configuration_initiale.
 * @param regle Tableau d'entiers qui doivent etre compris entre 0 et nb_etat-1 (ce tableau sera copié).
 * @param taille_regle Taille du tableau contenat la règle.
 * @param transition voir automate_cellulaire::transition pour plus de détails.
 * @param nb_generation Nombre de generations a creer.
 * @return un pointeur vers un struct automate_cellullaire, NULL en cas d'erreur.
 */
ptr_ac creer_automate_cellulaire(int nb_etats, int* configuration_initiale,int nb_cellules,
                                 int* regle, int taille_regle, int (*transition)(int*, int),
                                 int nb_generation){

    for (int i =0; i<nb_cellules ; i++) {
        if (configuration_initiale[i] < 0 || configuration_initiale[i] >= nb_etats) {
            fprintf(stderr,
                    "Une des cellules de la configuration initiale a un état négatif ou supérieur ou égal a %d\n",
                    nb_etats);
            return NULL;
        }
    }

    for (int i =0; i<taille_regle ; i++) {
        if (regle[i] < 0 || regle[i] >= nb_etats) {
            fprintf(stderr,
                    "Un états dans la règle est négatif ou supérieur ou égal a %d\n",
                    nb_etats);
            return NULL;
        }
    }

    ptr_ac a = (ptr_ac)malloc(sizeof(struct automate_cellulaire));
    if(a == NULL) return NULL;

    a->transition = transition;
    a->nb_generation = 1;
    a->nb_etats = nb_etats;
    a->nb_cellules = nb_cellules;
    a->taille_regle = taille_regle;

    a->regle = (int*)malloc(taille_regle*sizeof(int)); // copie de la regle par sécurité
    if(a->regle == NULL) {detruire_automate_cellulaire(a); return NULL;}
    memcpy(a->regle,regle, taille_regle*sizeof(int));

    a->cellules = (int**)calloc(nb_generation,sizeof(int*));
    if(a->cellules == NULL) {detruire_automate_cellulaire(a); return NULL;}

    if(configuration_initiale == NULL) {
        a->cellules[0] = (int *)calloc(nb_cellules, sizeof(int));
        if(a->cellules[0] == NULL) {detruire_automate_cellulaire(a); return NULL;}
    }else{
        a->cellules[0] = (int *)malloc(nb_cellules*sizeof(int));
        if(a->cellules[0] == NULL) {detruire_automate_cellulaire(a); return NULL;}
        memcpy(a->cellules[0], configuration_initiale, nb_cellules*sizeof(int)); // copie de la ligne de cellules par sécurité
    }

    while (a->nb_generation < nb_generation) {
        if(!generation_suivante(a)) {detruire_automate_cellulaire(a); return NULL;}
    }

    return a;
}

/**
 * Lis un tableau d'entier de la forme "taille : i1 i2 i3 i4 i5 ..." depuis un fichier.
 * @param f Fichier depuis lequel lire le tableau.
 * @param tab Pointeur vers le pointeur qui va recevoir le tableau lu.
 * @param taille Pointeur vers un entier ou la taille du tableau lu sera écrite.
 * @param nom_tableau Nom du tableau a lire, sert uniquement a l'affichage des erreurs.
 * @return true si la lecture s'est bien passé, false sinon.
 */
bool lire_tableau(FILE* f, int** tab, int* taille, char* nom_tableau){
    char c = 0;

    if(!fscanf(f,"%d", taille)){
        fprintf(stderr ,"caractère inattendu trouvé a la place de la taille de %s\n", nom_tableau);
        return false;
    }

    while((c = fgetc(f)) == ' ');
    if(c != ':') { // verification de la présence du :
        fprintf(stderr ,"caractère inattendu après la taille de %s\n", nom_tableau);
        return false;
    }

    *tab = (int*)malloc(*taille*sizeof(int));
    if(*tab == NULL) return false;

    for (int i = 0; i < *taille ; ++i){
        if(!fscanf(f,"%d", (*tab) + i)) {
            fprintf(stderr, "caractère inattendu lors de la lecture de %s\n", nom_tableau);
            free(*tab);
            *tab = NULL;
            return false;
        }
    }
    return true;
}

/**
 * Lis un fichier contenant les infos d'un automate cellulaire.
 * @param Chemin chemin vers le fichier .ac.
 * @param transition Fonction permettant de calculer l'etat d'une cellule à la génération suivante (voir automate_cellulaire pour plus d'info).
 * @param nb_generations nombre de générations de cellules a creer.
 * @return L'automate cellulaire chargé depuis le fichier intique, NULL si il y a eu une erreur de lecture.
 */
ptr_ac creer_automate_cellulaire_depuis_fichier(const char* chemin, int (*transition)(int*, int), int nb_generations){
    int *config = NULL, *regle = NULL;
    int taille_config = 0, taille_regle = 0 , nb_etats = 0;

    char c = 0;
    enum etat {NB_ETAT, REGLE, CONFIG, ERREUR, FIN};
    enum etat etat = NB_ETAT;
    bool lire = true;

    FILE* file = fopen(chemin,"r");
    if (file == NULL){
        fprintf(stderr ,"Impossible d'ouvrir le fichier %s\n", chemin);
        return NULL;
    }

    while (lire) {
        while ((c = fgetc(file)) == ' ' || c == '\n');

        if(etat != FIN && c == EOF){
            fprintf(stderr ,"Fichier %s incomplet\n", chemin);
            etat = ERREUR;
            break;
        }
        else if (c == '#') { //si on trouve un # (commentaire) on passe a la ligne suivante
            while ((c = fgetc(file)) != '\n' && c != EOF);
        }
        else{
            fseek(file, -1, SEEK_CUR);
            switch (etat){
                case NB_ETAT:
                    etat = REGLE;
                    if(!fscanf(file,"%d",&nb_etats)){
                        fprintf(stderr ,"caractère inattendu trouvé a la place du nombre d'états\n");
                        etat = ERREUR;
                    }
                    break;

                case REGLE:
                    etat = CONFIG;
                    if(!lire_tableau(file, &regle, &taille_regle, "la règle")) etat = ERREUR;
                    break;

                case CONFIG:
                    etat = FIN;
                    if(!lire_tableau(file, &config, &taille_config, "la configuration initiale")) etat = ERREUR;
                    break;

                case FIN:
                    if(c != EOF){
                        fprintf(stderr ,"caractère inattendu '%c' trouvé après la configuration initiale dans %s\n",c, chemin);
                        etat = ERREUR;
                    }
                    lire = false;
                    break;

                case ERREUR:
                    lire = false;
            }
        }
    }

    ptr_ac a = NULL;
    if (etat != ERREUR)
        a = creer_automate_cellulaire(nb_etats, config, taille_config, regle, taille_regle, transition, nb_generations);

    fclose(file);
    if(regle != NULL) free(regle);
    if(config != NULL) free(config);
    return a;
}

/**
 * Affiche un automate cellulaire.
 * @param a Automate a afficher.
 * @param afficher_cellules Fonction d'affichage de tableau qui prend en paramètre dans l'ordre : la matrice a afficher
 * (ici un tableau de ligne de cellules), la taille de ce tableau, le numéro de la génération des cellules.
 */
void afficher_automate_cellulaire(const ptr_ac a,void (*afficher_cellules)(int**, int, int, int)){
    (*afficher_cellules)(a->cellules,a->nb_cellules,a->nb_generation, a->nb_etats);
}

/**
 * Detruit un automate cellulaire.
 * @param a Automate a detruire.
 */
void detruire_automate_cellulaire(ptr_ac a){
    for(int i = 0; i < a->nb_generation; i++){
        free(a->cellules[i]);
    }
    if(a->regle != NULL) free(a->regle);
    if(a->cellules != NULL) free(a->cellules);
    free(a);
}

/////////////////////// FONCTIONS DE TRANSITION PREDEFINIES ///////////////////////

/**
 * Règle de transition de binaire.
 *
 * !!! ATTENTION !!! si la lecture de la règle a lieu depuis un fichier grâce a creer_automate_cellulaire_depuis_fichier,
 * pour par exemple utiliser la regle 30 (00011110) la regle devra être écrite comme suit dans le fichier "8 : 0 1 1 1 1 0 0 0"
 * c 'est a dire a l'envers car le 1er chiffre rencontré sera a l'indice 0 du tableau ou est stocké la règle.
 * @param voisnage Tableau des états des cellules voisines.
 * @param taille_regle Taille de la règle de transition.
 * @return L'indice ou aller chercher l'état suivant dans la règle.
 */
int transition_binaire(int* voisnage, int taille_regle){
    int i = 0;

    for(int j = 0; j < TAILLE_VOISINAGE; j++)
        i += (voisnage[j] == 0)? 0 : pow(2,j);

    return i;
}

/**
 * Règle de transition de somme d'un automate.
 * @param voisnage Tableau des états des cellules voisines.
 * @param taille_regle Taille de la règle de transition.
 * @return L'indice ou aller chercher l'état suivant dans la règle.
 */
int transition_somme(int* voisnage, int taille_regle){
    return voisnage[0]+voisnage[1]+voisnage[2];
}

/////////////////////// FONCTIONS D'AFFICHAGE PREDEFINIES ///////////////////////

/**
 * Méthode d'affichage en mode terminal d'un automate à maximum 4 états.
 * A utiliser avec afficher_automate_cellulaire.
 * @param cellules Matrice des cellules de l'automate.
 * @param nb_cellules Largeur de la matrice.
 * @param nb_generations Hauteur de la matrice.
 * @param nb_etats Nombre d'états.
 * @see afficher_automate_cellulaire
 */
void affichage_console_automate_4_etats_max(int** cellules, int nb_cellules, int nb_generations, int nb_etats){
    for(int j = 0; j < nb_generations; j++) {
        printf("|%d\t|", j+1);
        for (int i = 0; i < nb_cellules; i++) {
            switch (cellules[j][i]) {
                case 0:
                    printf(" ");
                    break;
                case 1:
                    printf("X");
                    break;
                case 2:
                    printf("Y");
                    break;
                case 3:
                    printf("Z");
                    break;
            }
        }
        printf("\n");
    }
}

/**
 * Crée une image à partir d'un automate et ses générations suivantes.
 * A utiliser avec afficher_automate_cellulaire.
 * @param cellules Matrice des cellules de l'automate.
 * @param nb_cellules Largeur de la matrice.
 * @param nb_generations Hauteur de la matrice.
 * @param nb_etats nombre d'états.
 * @see afficher_automate_cellulaire
 */
void affichage_image_automate(int** cellules, int nb_cellules, int nb_generations, int nb_etats){
    ptr_img img = tableau_vers_image_pgm((unsigned int **)cellules, nb_cellules, nb_generations, nb_etats-1, P5);
    enregistrer_image_pgm(img, "automate.pgm");
    detruire_image_pgm(img);
}
#include "image_pgm.h"

/**
 * @struct image_pgm
 * @brief Représente une image en niveaux de gris.
 */
struct image_pgm
{
	unsigned int largeur; /*!< Largeur en pixel d'une image PGM. */
    unsigned int hauteur; /*!< Hauteur en pixel d'une image PGM. */
    unsigned int valeur_max; /*!< Valeur maximale que peut prendre un pixel de l'image PGM. */
    format_pgm nb_magique; /*!< Donne le type d'encodage de l'image et détermine si celle-ci est en couleur ou non. */
	unsigned int** pixels; /*!< Matrice ou est stocké la valeur de chaque pixels de l'image. */
};

/**
 * Initialise une image pgm, utile uniquement pour eviter la redondance du code dans les differentes fonctions de création
 * d'image pgm qui suivent.
 * ATTENTION !!! ELLE initialise le tableau qui va contenir les lignes de pixels mais pas les lignes en elle même. C'est
 * pour cela qu'elle n'est pas déclarée dans image_pgm.h. Cette fonction sert d'ailleurs uniquement pour éviter la redondance
 * de code dans ce fichier.
 * @param largeur Largeur de l'image a creer.
 * @param hauteur Hargeur de l'image a creer.
 * @param valeur_max Valeur maximale de l'image a creer.
 * @param nb_magique Type d'encodage de l'image.
 * @return un pointeur vers le struct image_pgm créé, NULL si un prolème est survenu.
 */
ptr_img initialiser_image_pgm(unsigned int largeur, unsigned int hauteur, unsigned int valeur_max, format_pgm nb_magique){
    if (valeur_max > VALEUR_MAX_PIXEL){
        fprintf(stderr, "ERREUR : valeur_max superieur a %d", VALEUR_MAX_PIXEL);
        return NULL;
    }

    ptr_img img = (ptr_img) malloc(sizeof(struct image_pgm));
    if(img == NULL) return NULL;

    img->largeur = largeur;
    img->hauteur = hauteur;
    img->valeur_max = valeur_max;
    img->nb_magique = nb_magique;

    img->pixels = (unsigned int**) malloc(img->hauteur*sizeof(unsigned int*));
    if(img->pixels == NULL) {detruire_image_pgm(img); return NULL;}

    return img;
}

/**
 * Lis l'entete d'une image pgm
 * @param fichier fichier contenant l'image a lire
 * @param largeur pointeur dont la valeur associee va etre remplacée par la largeur le l'image écrite dans le fichier
 * @param hauteur pointeur dont la valeur associee va etre remplacée par la hauteur le l'image écrite dans le fichier
 * @param valeur_max pointeur dont la valeur associee va etre remplacée par la valeur maximale le l'image écrite dans le fichier
 * @param nb_magique pointeur dont la valeur associee va etre remplacée par le nombre magique le l'image écrite dans le fichier
 * @return true si la lecture s'est bien passé, false sinon
 */
bool lire_entete_image_pgm(const FILE* fichier, unsigned int* largeur, unsigned int* hauteur, unsigned int* valeur_max, format_pgm* nb_magique) {

    enum etats { INIT, MAGIC_NUMBER_FOUND, WIDTH_HEIGHT_FOUND, MAX_VAL_FOUND, ERROR };
    enum etats etat = INIT ; 
    const int MAX_LENGTH = 4096;

    char* buf = malloc(sizeof(char)*MAX_LENGTH);
    if(buf == NULL) return false;
    bool lireEntete = true;

    while(lireEntete) {

        fgets(buf, MAX_LENGTH, (FILE *) fichier);
        switch(etat) {

            case INIT:
                if(strncmp(buf,"P",1) == 0) etat = MAGIC_NUMBER_FOUND;
                if(buf[1] != '2' && buf[1] != '5'){
                	etat = ERROR;
                	fprintf(stderr, "le format P%c n'est pas reconu par cette librairie\n", buf[1]);
                	lireEntete = false;
                }
                else *nb_magique = ((int)buf[1]-(int)'0' == 5)? P5 : P2;
                break;
            case MAGIC_NUMBER_FOUND:
                while(strncmp(buf,"#",1) == 0) 
                    fgets(buf, MAX_LENGTH, (FILE *) fichier);
                if(sscanf(buf,"%u %u", largeur, hauteur) == 2) etat = WIDTH_HEIGHT_FOUND; 
                else lireEntete = false;
                break;
            case WIDTH_HEIGHT_FOUND:
                if(sscanf(buf,"%u", valeur_max) == 1) {
                    if(*valeur_max == 0) fprintf(stderr, "Entete PGM : une valeur max de 0 n'est pas autorisee.\n");
                    else etat = MAX_VAL_FOUND;
                }
                lireEntete = false;
                break;
            default:
                etat = ERROR;
                lireEntete = false;
        }

    }

    free(buf);

    if(etat != MAX_VAL_FOUND) {
        fprintf(stdout, "Entete PGM incorrect, evaluation terminee a l'etat %d\n", etat);
        return false;
    }

    return true;

}

/**
 * Lis le corps d'une l'image pgm
 * @param f fichier ou se trouve l'image dont il ne reste que le corps a lire
 * @param img image_pgm dans lequel va être chargé l'image
 * @return true si la lecture s'est bien passe, false sinon
 */
bool lire_corps_image_pgm(FILE* f, ptr_img img){
	img->pixels = (unsigned int**) calloc(img->hauteur,sizeof(unsigned int*));
	if(img->pixels == NULL) return NULL;

	if(img->nb_magique == P2){
		for(unsigned int y = 0; y < img->hauteur ; y++){
			img->pixels[y] = (unsigned int*) malloc(img->largeur*sizeof(unsigned int));
			if(img->pixels[y] == NULL) return false; //en cas d'erreur la memoire allouée par la 1ere instruction de la fonction sera libere dans lire_image_pgm
			for(unsigned int x = 0; x < img->largeur ; x++){

			    if(fscanf(f,"%u",img->pixels[y] + x) == 0){
                    fprintf(stderr, "caractere inattendu ou la resolution ne correspond pas au nombre de lignes/colonnes\n");
                    return false;
                }

				if(img->pixels[y][x] > img->valeur_max){
					fprintf(stderr, "un des pixel de l'image depasse la valeur max\n");
					return false;
				}
			}
		}
	}
	else{
		const int OCTETS_A_LIRE = img->valeur_max < 256 ? 1 : 2;

	    for(unsigned int y = 0; y < img->hauteur; y++) {
	    	img->pixels[y] = (unsigned int*) malloc(img->largeur*sizeof(unsigned int));
	    	if(img->pixels[y] == NULL) return false; //en cas d'erreur la memoire allouée par les autres iterations seront libere dans lire_image_pgm
	        for(unsigned int x = 0; x < img->largeur; x++) {
	            if(fread(&img->pixels[y][x], OCTETS_A_LIRE, 1, f) == 0)
	                return false;
	        }
	    }
	}
	return true;
}

/**
 * Lis un fichier image PGM
 * @param path chemin de l'image a lire
 * @return un pointeur vers un struct image_pgm représentant l'image chargée, NULL si un prolème est survenu.
 */
ptr_img lire_image_pgm(char* path){
    FILE* f = fopen(path,"r");
    if(f == NULL){
        fprintf(stderr, "Impossible d'ouvrir l'image %s\n",path);
        return NULL;
    }

    unsigned int largeur = 0, hauteur = 0, valeur_max = 0, nb_magique = 0;

    if(!lire_entete_image_pgm(f, &largeur, &hauteur, &valeur_max, &nb_magique)){
        fclose(f);
        return NULL;
    }

    ptr_img img = initialiser_image_pgm(largeur, hauteur, valeur_max, nb_magique);
    if(img == NULL){ fclose(f); return NULL;}

    if(!lire_corps_image_pgm(f,img)){
        fclose(f);
        detruire_image_pgm(img);
        return NULL;
    }

    fclose(f);
    return img;
}

/**
 * Crée une image noire.
 * @param largeur Largeur de l'image a creer.
 * @param hauteur Hargeur de l'image a creer.
 * @param valeur_max Valeur maximale de l'image a creer.
 * @param nb_magique Type d'encodage de l'image.
 * @return un pointeur vers un struct image_pgm repésentant une image noire, NULL si un prolème est survenu.
 */
ptr_img creer_image_pgm_noire(unsigned int largeur, unsigned int hauteur, unsigned int valeur_max, format_pgm nb_magique){
    ptr_img img = initialiser_image_pgm(largeur, hauteur, valeur_max, nb_magique);

    for(unsigned int y = 0; y < img->hauteur; y++) {
        img->pixels[y] = (unsigned int *) calloc(img->largeur, sizeof(unsigned int));
        if(img->pixels[y] == NULL) {detruire_image_pgm(img); return NULL;}
    }

    return img;
}

/**
 * Copie une image PGM.
 * @param img Image a copier.
 * @return un pointeur vers la copie e img, NULL si un problème est survenu.
 */
ptr_img copier_image_pgm(const ptr_img img){ //verifier necessité de const ptr_img const
    ptr_img cpy = initialiser_image_pgm(img->largeur, img->hauteur, img->valeur_max, img->nb_magique);

    for(unsigned int y = 0; y < cpy->hauteur; ++y){
        cpy->pixels[y] = malloc(cpy->largeur*sizeof(unsigned int));
        if(cpy->pixels[y] == NULL){detruire_image_pgm(cpy); return NULL;}
        memcpy(cpy->pixels[y],img->pixels[y],cpy->largeur*sizeof(unsigned int));
    }

    return cpy;
}

/**
 * Acceceur pour la largeur d'une image PGM.
 * @param img Image dont on veut la largeur.
 * @return La largeur de l'image.
 */
unsigned int get_largeur_image_pgm(ptr_img img){
    return img->largeur;
}

/**
 * Acceceur pour la hauteur d'une image PGM.
 * @param img Image dont on veut la hauteur.
 * @return La hauteur de l'image.
 */
unsigned int get_hauteur_image_pgm(ptr_img img){
    return img->hauteur;
}

/**
 * Acceceur pour la valeur maximale d'une image PGM.
 * @param img Image dont on veut la valeur maximale.
 * @return La valeur maximale de l'image.
 */
unsigned int get_valeur_max_image_pgm(ptr_img img){
    return img->valeur_max;
}


 /**
  * Acceceur pour la valeur d'un pixel de l'image PGM.
  * @param x Coordonnée horizontale du pixel dont on veut la valeur.
  * @param y Coordonnée verticale du pixel dont on veut la valeur.
  * @param img Image dont on veut la valeur du pixel demandé.
  * @return La valeur du pixel demandé.
  */
unsigned int get_pixel_image_pgm(unsigned int x, unsigned int y, ptr_img img){
     return img->pixels[y][x];
}

/**
 * Mutateur pour la valeur d'un pixel de l'image PGM.
 * @param x Coordonnée horizontale du pixel.
 * @param y Coordonnée verticale du pixel.
 * @param valeur Nouvelle valeur du pixel.
 * @param img Image dont on veut changer la valeur d'un pixel.
 */
void set_pixel_image_pgm(unsigned int x, unsigned  int y, unsigned int valeur, ptr_img img){
    img->pixels[y][x] = valeur;
}

/**
 * Convertis un tableau en image pgm.
 * @param tab Tableau a converitr en image PGM (cette matrice est copié dans l'image).
 * @param largeur Largeur du tableau a convertir.
 * @param hauteur Hargeur du tableau a convertir.
 * @param valeur_max Valeur maximale du tableau a convertir.
 * @param nb_magique Type d'encodage de l'image a convertir.
 * @return un pointeur vers un struct image_pgm représentant l'image du tableau, NULL si un problème est survenu.
 */
ptr_img tableau_vers_image_pgm(unsigned int** tab, unsigned int largeur, unsigned int hauteur, unsigned int valeur_max, format_pgm nb_magique){
    ptr_img img = initialiser_image_pgm(largeur, hauteur, valeur_max, nb_magique);

    for(unsigned int y = 0; y < img->hauteur; y++){
        img->pixels[y] = (unsigned int *) calloc(img->largeur, sizeof(unsigned int));
        if(img->pixels[y] == NULL) {detruire_image_pgm(img); return NULL;}
        memcpy(img->pixels[y],tab[y],img->largeur* sizeof(unsigned int));
    }

    return img;
}

/**
 * Enregistre une image_pgm dans un fichier.
 * @param pgm Image a enregistrer.
 * @param chemin Fichier dans lequel enregistrer l'image (crée si inexistant).
 * @return true si l'enregistrement s'est bien passé, fals sinon.
 */
bool enregistrer_image_pgm(ptr_img img, char* path){
	FILE* f = fopen(path,"w");
    if(f == NULL){
        fprintf(stderr, "Impossible d'enregistrer l'image dans %s\n",path);
        return false;
    }

	fputs((img->nb_magique == P5)? "P5\n" : "P2\n", f);

	fprintf(f, "%u %u\n%u\n", img->largeur, img->hauteur, img->valeur_max);

	if(img->nb_magique == P2){
		for (unsigned int y = 0; y < img->hauteur; ++y){
			for (unsigned int x = 0; x < img->largeur; ++x)
				fprintf(f," %u",img->pixels[y][x]);
			fputc('\n', f);
		}
	}
	else{
		const int OCTETS_A_ECRIRE = img->valeur_max < 256 ? 1 : 2;
		for (unsigned int y = 0; y < img->hauteur; ++y)
			for(unsigned int x = 0; x < img->largeur; ++x)
				fwrite(img->pixels[y]+x,1,OCTETS_A_ECRIRE, f);
	}
	
	fclose(f);
	return true;
}

/**
 * Libère la mémoire d'une image PGM.
 * @param img Image a détruire.
 */
void detruire_image_pgm(ptr_img img){
	for(unsigned int y = 0; y < img->hauteur ; y++)
		free(img->pixels[y]);

	free(img->pixels);
	free(img);
}

/**
 * Filtre les pixels d'une image en fonction du filtre passé en paramètre et enregistre le résultat dans un fichier.
 * @param img Image a filtrer (elle ne sera pas modifiée).
 * @param filtre Filtre a appliquer a l'image, il prend en paramètre une copie de l'image a filtrer.
 * @param image_sortie Fichier ou sera enregistré le résultat.
 * @return true si tout s'est bien passé, false sinon.
 */
bool filtrer_image_pgm(ptr_img img, void(*filtre)(struct image_pgm img), char* image_sortie){
    ptr_img cpy = copier_image_pgm(img);
    if(cpy == NULL) return false;
	(*filtre)(*cpy);
	bool res = enregistrer_image_pgm(cpy, image_sortie);
	detruire_image_pgm(cpy);
    return res;
}

//////////////////////////// FILTRES ////////////////////////////

/**
 * Applique un filtre négatif sur une image PGM.
 * Peut être passé en paramètre de la fonction filtrer_image_pgm.
 * @param img image a filtrer.
 * @see filtrer_image_pgm
 */
void filtre_negatif(struct image_pgm img){
	for(unsigned int y = 0; y < img.hauteur; y++)
		for(unsigned int x = 0; x < img.largeur; x++)
			img.pixels[y][x] = img.valeur_max - img.pixels[y][x];
}
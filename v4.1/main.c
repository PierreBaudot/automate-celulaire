#include "automate_cellulaire.h"

/*! \mainpage
 * Pour lancer le programme avec le fichier main.c fourni vous devez d'abord compiler le code avec la commande make depuis un
 * terminal dans le fichier contenant le Makefile du projet, et ensuite lancer la commande :
 *
 *      ./main [fichier.ac] [nb_generations]
 *
 * fichier.ac        est le chemin vers un fichier contenant la configuration d'un automate cellulaire.\n
 * nb_generations    est le nombre de générations souhaitées a calculer.
 *
 *
 * Le résultat sera enregistré dans une image appelé automate.pgm dans le répértoire ou a été ouvert le terminal.
 *
 *
 * \n\n\n\n
 *
 *
 * Ce projet et ses différentes versions sont disponibles sur le dépot framagit :
 * https://framagit.org/PierreBaudot/automate-celulaire.git
 *
 * Créé par Mathis Querault et Pierre Baudot.
 */

/**
 * Programme principal, Calcule les argv[2] générations succesives de l'automate cellulaire contenu dans le fichier argv[1]
 * et affichie le résultat d'une image qui est enregistré sous le nom "automate.pgm".
 */
int main(int argc, char const *argv[]){

    if(argc != 3){
        fprintf(stderr, "Erreur : le nombre d'arguments est invalide\n");
        return EXIT_FAILURE;
    }

    int nb_generations = 0;
    if(!sscanf(argv[2], "%d", &nb_generations)){
        fprintf(stderr, "nombre de generation invalide %s\n", argv[1]);
        return EXIT_FAILURE;
    }

    ptr_ac a = creer_automate_cellulaire_depuis_fichier(argv[1], &transition_somme, nb_generations);
    if(a == NULL){
        fprintf(stderr, "erreur lors de la lecture de %s\n", argv[1]);
        return EXIT_FAILURE;
    }

    afficher_automate_cellulaire(a,&affichage_image_automate);
    detruire_automate_cellulaire(a);

    printf("L'automate obtenu a été enregistré dans l'image automate.pgm\n");
    return EXIT_SUCCESS;
}
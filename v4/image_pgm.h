#ifndef IMAGE_H
#define IMAGE_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define VALEUR_MAX 65536

struct image_pgm;

typedef enum { P2, P5 } format_pgm;
typedef struct image_pgm* ptr_img;

ptr_img lire_image_pgm(char* path);
ptr_img creer_image_pgm_blanche(unsigned int largeur, unsigned int hauteur, unsigned int valeur_max, format_pgm nb_magique);
ptr_img tableau_vers_image_pgm(unsigned int** tab, unsigned int largeur, unsigned int hauteur, unsigned int valeur_max, format_pgm nb_magique);

bool enregistrer_image_pgm(ptr_img pgm, char* path);
void detruire_image_pgm(ptr_img img);
ptr_img copier_image_pgm(const ptr_img img);
void filtrer_image_pgm(ptr_img i, void(*filtre)(struct image_pgm i));

void filtre_negatif(struct image_pgm i);
void filtre_masque_horizontal(struct image_pgm img);
void filtre_masque_vertical(struct image_pgm img);

#endif
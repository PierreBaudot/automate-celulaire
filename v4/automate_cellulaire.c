#include "automate_cellulaire.h"

struct automate_cellulaire
{
	int nb_cellules;
	int nb_generation;
	int nb_etats;
    int *regle;
    int **cellules;
    int (*transition)(int*, int*);
};


/**
 * Calcule la generation suivante d'un automate cellulaire
 * @param a automate sur lequel passer a la generation suivante
 */
void generation_suivante(ptr_ac a){
    a->cellules[a->nb_generation] = (int*)malloc(a->nb_cellules*sizeof(int));

    int* voisins  = (int*) malloc(TAILLE_VOISINAGE*sizeof(int));

    for(int i = 0 ; i < a->nb_cellules; i++){
        voisins[2] = a->cellules[a->nb_generation-1][(i == 0)? a->nb_cellules-1 : i-1];
        voisins[1] = a->cellules[a->nb_generation-1][i];
        voisins[0] = a->cellules[a->nb_generation-1][(i+1)%a->nb_cellules];

        a->cellules[a->nb_generation][i] = a->transition(voisins,a->regle);
    }
    free(voisins);
    a->nb_generation++;
}

ptr_ac creer_automate_cellulaire(int nb_etats, int* configuration_initiale,int nb_cellules,
                                 int* regle, int taille_regle, int (*transition)(int*, int*),
                                 int nb_generation){
    ptr_ac a = (ptr_ac)malloc(sizeof(struct automate_cellulaire));
    a->nb_cellules = nb_cellules;
    a->regle = (int*)malloc(taille_regle*sizeof(int)); // copie de la regle par sécurité
    memcpy(a->regle,regle, taille_regle*sizeof(int));
    a->transition = transition;
    a->nb_etats = nb_etats;

    a->cellules = (int**)calloc(nb_generation,sizeof(int*));

    if(configuration_initiale == NULL) {
        a->cellules[0] = (int *) calloc(nb_cellules, sizeof(int));
    }else{
        a->cellules[0] = (int *)malloc(nb_cellules*sizeof(int));
        memcpy(a->cellules[0], configuration_initiale, nb_cellules*sizeof(int)); // copie de la ligne de cellules par sécurité

        for (int i =0; i<nb_cellules ; i++){
            if(a->cellules[0][i] < 0 || a->cellules[0][i] >= a->nb_etats){
                detruire_automate_cellulaire(a);
                return NULL;
            }
        }
    }

    a->nb_generation = 1;
    while (a->nb_generation < nb_generation) {
        generation_suivante(a);
    }

	return a;
}

/**
 * Lis une suite de chiffres (sans espaces) et le stocke dans pTab Fonction servant a éviter la redondance de code dans
 * lire_fichier_automate_cellulaire
 * @param pFile
 * @param pTab
 * @param pCpt
 */
void recuperation_donnee(FILE* file, int** pTab, int* pCpt){
    int cpt = 0;
    int caractereActuel;
    while ((caractereActuel = fgetc(file)) != '\n' && caractereActuel != ' ' && caractereActuel != '#' && caractereActuel != EOF ){
        cpt++;
    }
    if(cpt == 0) return;
    
    int* tab = (int*)malloc(cpt* sizeof(int));
    if (caractereActuel == EOF) fseek(file,-cpt,SEEK_CUR);
    else fseek(file,-cpt-1,SEEK_CUR);

    for (int i = 0; i < cpt; ++i) {
        caractereActuel = fgetc(file);
        tab[i] = caractereActuel-'0';
    }
    *pCpt=cpt;
    *pTab=tab;
}

ptr_ac lire_fichier_automate_cellulaire(char* chemin, int (*transition)(int*, int*), int nb_generations){
    int *_config = NULL, *_regle = NULL;
    int _cptConfig = 0, _cptRegle = 0;

    FILE* file = fopen(chemin,"r");
    if (file == NULL){
        fprintf(stderr ,"Impossible d'ouvrir le fichier %s\n", chemin);
        return NULL;
    }

    int caractereActuel;
    while ((caractereActuel = fgetc(file)) != EOF){

        if(caractereActuel == '#') {
            while ((caractereActuel = fgetc(file)) != '\n' || caractereActuel != EOF);
            if(caractereActuel == EOF) break;
        }
        else if(caractereActuel == 'c' && _config == NULL){
            printf("conf\n");
            if(_config != NULL) {
                fprintf(stderr, "configuration initiale en double dans le fichier %s\n", chemin);
                free(_config); fclose(file);
                if(_regle != NULL) free(_regle);
                return NULL;
            }
            fgetc(file); // Lecture de l'espace
            recuperation_donnee(file,&_config,&_cptConfig);
        }
        else if(caractereActuel == 'r'){
            printf("regle\n");
            if(_regle != NULL) {
                fprintf(stderr, "regle en double dans le fichier %s\n", chemin);
                free(_regle); fclose(file);
                if(_config != NULL) free(_config);
                return NULL;
            }
            fgetc(file); // Lecture de l'espace
            recuperation_donnee(file,&_regle,&_cptRegle);
        }
    }; // On continue tant que fgetc n'a pas retourné EOF (fin de fichier)

    //recherche de la valeur max d'un etat pour gerer le nombre d'états de l'automate
    int nb_etats = _config[0];
    for (int i = 1; i < _cptConfig; i++){
        if (_config[i] > nb_etats) nb_etats  = _config[i];
    }
    for (int i = 0; i < _cptRegle; i++){
        if (_regle[i] > nb_etats) nb_etats  = _regle[i];
    }
    nb_etats=nb_etats+1; //car 0 est un etat
    
    
    fclose(file);

    ptr_ac ac = NULL;
    if(_regle == NULL || _config == NULL)
        fprintf(stderr, "regle ou configuration initialle manquante dans le fichier %s\n", chemin);
    else
        ac = creer_automate_cellulaire(nb_etats, _config, _cptConfig, _regle, _cptRegle, transition, nb_generations);

    if(_config != NULL) free(_config);
    if(_regle != NULL) free(_regle);
    return ac;
}

void afficher_automate_cellulaire(const ptr_ac a,void (*afficher_cellules)(int**, int, int, int)){
    (*afficher_cellules)(a->cellules,a->nb_cellules,a->nb_generation, a->nb_etats);
}

void detruire_automate_cellulaire(ptr_ac a){
    for(int i = 0; i < a->nb_generation && a->cellules[i] != NULL; i++){
        free(a->cellules[i]);
    }
	free(a->regle);
	free(a->cellules);
	free(a);
}
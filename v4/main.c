#include "automate_cellulaire.h"
#include "image_pgm.h"
#include <string.h>
#include <stdio.h>

#define NOM_FICHIER "config_automate.ac"

void afficher_cellules_4_etats(int** tab, int largeur, int hauteur, int val_max){
    for(int j = 0; j < hauteur; j++) {
        printf("|%d\t|", j+1);
        for (int i = 0; i < largeur; i++) {
            switch (tab[j][i]) {
                case 0:
                    printf(" ");
                    break;
                case 1:
                    printf("X");
                    break;
                case 2:
                    printf("Y");
                    break;
                case 3:
                    printf("Z");
                    break;
            }
        }
        printf("\n");
    }
}

void tableau_vers_image(int** tab, int largeur, int hauteur, int val_max){
    ptr_img img = tableau_vers_image_pgm((unsigned int **)tab, largeur, hauteur, val_max, P5);
    enregistrer_image_pgm(img, "test.pgm");
    detruire_image_pgm(img);
}

int transition_somme(int* voisnage, int* regle){
    return regle[voisnage[0]+voisnage[1]+voisnage[2]];
}

int main(int argc, char const *argv[])
{
    int ite = 16;

    ptr_ac a = lire_fichier_automate_cellulaire(NOM_FICHIER, &transition_somme, ite);
    if(a == NULL){
        fprintf(stderr, "erreur lors de la lecture de %s\n", NOM_FICHIER);
        return EXIT_FAILURE;
    }
    afficher_automate_cellulaire(a,&afficher_cellules_4_etats);
    afficher_automate_cellulaire(a,&tableau_vers_image);
    detruire_automate_cellulaire(a);
    return EXIT_SUCCESS;
}
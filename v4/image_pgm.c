#include "image_pgm.h"

struct image_pgm
{
	unsigned int largeur, hauteur, valeur_max;
	format_pgm nb_magique;
	unsigned int** pixels;
};

/**
 * Lis l'entete d'une image pgm
 * @param fichier fichier contenant l'image a lire
 * @param largeur pointeur dont la valeur associee va etre remplacée par la largeur le l'image écrite dans le fichier
 * @param hauteur pointeur dont la valeur associee va etre remplacée par la hauteur le l'image écrite dans le fichier
 * @param valeur_max pointeur dont la valeur associee va etre remplacée par la valeur maximale le l'image écrite dans le fichier
 * @param nb_magique pointeur dont la valeur associee va etre remplacée par le nombre magique le l'image écrite dans le fichier
 * @return true si la lecture s'est bien passé, false sinon
 */
bool lire_entete_image_pgm(const FILE* fichier, unsigned int* largeur, unsigned int* hauteur, unsigned int* valeur_max, format_pgm* nb_magique) {

    enum etats { INIT, MAGIC_NUMBER_FOUND, WIDTH_HEIGHT_FOUND, MAX_VAL_FOUND, ERROR };
    enum etats etat = INIT ; 
    const int MAX_LENGTH = 4096;

    char* buf = malloc(sizeof(char)*MAX_LENGTH);
    if(buf == NULL) return false;
    bool lireEntete = true;

    while(lireEntete) {

        fgets(buf, MAX_LENGTH, (FILE *) fichier);
        switch(etat) {

            case INIT:
                if(strncmp(buf,"P",1) == 0) etat = MAGIC_NUMBER_FOUND;
                if(buf[1] != '2' && buf[1] != '5'){
                	etat = ERROR;
                	fprintf(stderr, "le format P%c n'est pas reconu par cette librairie\n", buf[1]);
                	lireEntete = false;
                }
                else *nb_magique = ((int)buf[1]-(int)'0' == 5)? P5 : P2;
                break;
            case MAGIC_NUMBER_FOUND:
                while(strncmp(buf,"#",1) == 0) 
                    fgets(buf, MAX_LENGTH, (FILE *) fichier);
                if(sscanf(buf,"%u %u", largeur, hauteur) == 2) etat = WIDTH_HEIGHT_FOUND; 
                else lireEntete = false;
                break;
            case WIDTH_HEIGHT_FOUND:
                if(sscanf(buf,"%u", valeur_max) == 1) {
                    if(*valeur_max == 0) fprintf(stderr, "Entete PGM : une valeur max de 0 n'est pas autorisee.\n");
                    else etat = MAX_VAL_FOUND;
                }
                lireEntete = false;
                break;
            default:
                etat = ERROR;
                lireEntete = false;
        }

    }

    free(buf);

    if(etat != MAX_VAL_FOUND) {
        fprintf(stdout, "Entete PGM incorrect, evaluation terminee a l'etat %d\n", etat);
        return false;
    }

    return true;

}

/**
 * lis le corps d'une l'image pgm
 * @param f fichier ou se trouve l'image dont il ne reste que le corps a lire
 * @param img image_pgm dans lequel va être chargé l'image
 * @return true si la lecture s'est bien passe, false sinon
 */
bool lire_corps_image_pgm(FILE* f, ptr_img img){ //TODO verifier lecture (EOF)
	img->pixels = (unsigned int**) calloc(img->hauteur,sizeof(unsigned int*));
	if(img->pixels == NULL) return NULL;

	if(img->nb_magique == P2){
		int c; 
		char* error_msg = NULL;
		for(unsigned int y = 0; y < img->hauteur ; y++){
			img->pixels[y] = (unsigned int*) calloc(img->largeur,sizeof(unsigned int));
			if(img->pixels[y] == NULL) return false; //en cas d'erreur la memoire allouée par la 1ere instruction de la fonction sera libere dans lire_image_pgm
			for(unsigned int x = 0; x < img->largeur ; x++){

				while((c = fgetc(f)) == ' ');
				while(c != ' ' && c != '\n'){
					img->pixels[y][x] = img->pixels[y][x]*10 + (c - '0');
					c = fgetc(f);
				}

				if(img->pixels[y][x] > img->valeur_max){
					fprintf(stderr, "un des pixel de l'image_pgm depasse la valeur max\n");
					return false;
				}

				if(c == '\n' && x != img->largeur-1){
					fprintf(stderr, "la largeur d'une des lignes de pixels de l'image_pgm est trop petite par rapport a la resolution donnee\n");
					return false;
				}
			}

			if(c == ' ')
				while((c = fgetc(f)) == ' ');

			if(c != '\n' && c != EOF){
				fprintf(stderr, "la largeur d'une des lignes de pixels de l'image_pgm est trop grande par rapport a la resolution donnee\n");
				return false;
			}
		}
	}
	else{
		const int OCTETS_A_LIRE = img->valeur_max < 256 ? 1 : 2;

	    for(unsigned int y = 0; y < img->hauteur; y++) {
	    	img->pixels[y] = (unsigned int*) malloc(img->largeur*sizeof(unsigned int));
	    	if(img->pixels[y] == NULL) return false; //en cas d'erreur la memoire allouée par les autres iterations seront libere dans lire_image_pgm
	        for(unsigned int x = 0; x < img->largeur; x++) {
	            if(fread(&img->pixels[y][x], OCTETS_A_LIRE, 1, f) == 0)
	                return false;
	        }
	    }
	}
	return true;
}


ptr_img lire_image_pgm(char* path){
	FILE* f = fopen(path,"r");

	ptr_img img = (ptr_img) malloc(sizeof(struct image_pgm));
	if(img == NULL){ fclose(f); return NULL;}

	if(!lire_entete_image_pgm(f, &(img->largeur), &(img->hauteur), &(img->valeur_max), &(img->nb_magique)) || !lire_corps_image_pgm(f,img)){
		fclose(f);
		detruire_image_pgm(img);
		return NULL;
	}
	
	fclose(f);
	return img;
}

/**
 * Initialise une image pgm, utile uniquement pour eviter la redondance du code dans les differentes fonctions de création
 * d'image pgm qui suivent, ATTENTION ELLE initialise le tableau qui va contenir les lignes de pixels mais pas les lignes
 * en elle même.
 */
ptr_img initialiser_image_pgm(unsigned int largeur, unsigned int hauteur, unsigned int valeur_max, format_pgm nb_magique){
    if (valeur_max > VALEUR_MAX){
        fprintf(stderr, "ERREUR : valeur_max superieur a %d", VALEUR_MAX);
        return NULL;
    }

    ptr_img img = (ptr_img) malloc(sizeof(struct image_pgm));
    if(img == NULL) return NULL;

    img->largeur = largeur;
    img->hauteur = hauteur;
    img->valeur_max = valeur_max;
    img->nb_magique = nb_magique;
    img->pixels = NULL;

    img->pixels = (unsigned int**) malloc(img->hauteur*sizeof(unsigned int*));
    if(img->pixels == NULL){
        detruire_image_pgm(img);
        return NULL;
    }

    return img;
}

ptr_img creer_image_pgm_blanche(unsigned int largeur, unsigned int hauteur, unsigned int valeur_max, format_pgm nb_magique){
    ptr_img img = initialiser_image_pgm(largeur, hauteur, valeur_max, nb_magique);

    for(unsigned int y = 0; y < img->hauteur; y++) {
        img->pixels[y] = (unsigned int *) calloc(img->largeur, sizeof(unsigned int));
        if(img->pixels[y] == NULL){
            detruire_image_pgm(img);
            return NULL;
        }
    }

    return img;
}

ptr_img copier_image_pgm(const ptr_img img){ //verifier necessité de const ptr_img const
    ptr_img cpy = initialiser_image_pgm(img->largeur, img->hauteur, img->valeur_max, img->nb_magique);

    for(unsigned int y = 0; y < cpy->hauteur; ++y){
        cpy->pixels[y] = malloc(cpy->largeur*sizeof(unsigned int));
        if(cpy->pixels[y] == NULL){
            detruire_image_pgm(cpy);
            return NULL;
        }
        memcpy(cpy->pixels[y],img->pixels[y],cpy->largeur*sizeof(unsigned int));
    }

    return cpy;
}

ptr_img tableau_vers_image_pgm(unsigned int** tab, unsigned int largeur, unsigned int hauteur, unsigned int valeur_max, format_pgm nb_magique){
    ptr_img img = initialiser_image_pgm(largeur, hauteur, valeur_max, nb_magique);

    for(unsigned int y = 0; y < img->hauteur; y++){
        img->pixels[y] = (unsigned int *) calloc(img->largeur, sizeof(unsigned int));
        if(img->pixels[y] == NULL){
            detruire_image_pgm(img);
            return NULL;
        }
        memcpy(img->pixels[y],tab[y],img->largeur* sizeof(unsigned int));
    }

    return img;
}

bool enregistrer_image_pgm(ptr_img img, char* path){ //TODO verifier ecriture
	FILE* f = fopen(path,"w");

	fputs((img->nb_magique == P5)? "P5\n" : "P2\n", f);

	fprintf(f, "%u %u\n%u\n", img->largeur, img->hauteur, img->valeur_max);

	if(img->nb_magique == P2){
		for (unsigned int y = 0; y < img->hauteur; ++y){
			for (unsigned int x = 0; x < img->largeur; ++x)
				fprintf(f," %u",img->pixels[y][x]);
			fputc('\n', f);
		}
	}
	else{
		const int OCTETS_A_ECRIRE = img->valeur_max < 256 ? 1 : 2;
		for (unsigned int y = 0; y < img->hauteur; ++y)
			for(unsigned int x = 0; x < img->largeur; ++x)
				fwrite(img->pixels[y]+x,1,OCTETS_A_ECRIRE, f);
	}
	
	fclose(f);
	return true;
}

void detruire_image_pgm(ptr_img img){
	for(unsigned int y = 0; y < img->hauteur ; y++)
		free(img->pixels[y]);

	free(img->pixels);
	free(img);
	img = NULL;
}

void filtrer(ptr_img img, void(*filtre)(struct image_pgm img)){
	(*filtre)(*img);
}

// FILTRES
void filtre_negatif(struct image_pgm img){
	for(unsigned int y = 0; y < img.hauteur; y++)
		for(unsigned int x = 0; x < img.largeur; x++)
			img.pixels[y][x] = img.valeur_max - img.pixels[y][x];
}

void filtre_masque_horizontal(struct image_pgm img){
	for(unsigned int y = 0; y < img.hauteur-1; y++)
		for(unsigned int x = 0; x < img.largeur; x++)
			img.pixels[y][x] = img.pixels[y+1][x] - img.pixels[y][x];
}

void filtre_masque_vertical(struct image_pgm img){
	for(unsigned int y = 0; y < img.hauteur; y++)
		for(unsigned int x = 1; x < img.largeur; x++)
			img.pixels[y][x] = img.pixels[y][x] - img.pixels[y][x-1];
}
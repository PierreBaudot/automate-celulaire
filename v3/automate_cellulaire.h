#ifndef AUTOMATE_CELLULAIRE_H
#define AUTOMATE_CELLULAIRE_H

#define TAILLE_VOISINAGE 3

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct automate_cellulaire;

typedef struct automate_cellulaire* ptr_ac;

/**
 * Crée un nouvel automate celulaire.
 *
 * @param nb_etats Nombre d'états que peut prendre une cellule.
 * @param configuration_initiale Tableau d'entiers (compris entre 0 et etat-1) de la premiere génération de cellules.
 * @param taille Taille du tableau configuration_initiale.
 * @param regle Tableau d'entiers qui doivent etre compris entre 0 et nb_etat-1.
 * @param transition Fonction permettant de calculer l'etat d'une cellule à la génération suivante qui prend en paramètre
 * dans l'ordre : 1) Un tableau de taille TAILLE_VOISINAGE contenant l'état des cellules (dans l'ordre d'apparition dans
 * la ligne) du voisinage de la cellule dont ont veut calculer l'état a la génération suivante. 2) la règle qui est
 * tableau d'entiers qui sont etre compris entre 0 et nb_etat-1.
 * Cette fonction doit retourner l'état de la cellule à la génération suivante.
 * @return un pointeur vers un struct automate_cellullaire
 */
ptr_ac creer_automate_cellulaire(int nb_etats, int* configuration_initiale,unsigned int taille, int* regle, unsigned int taille_regle, int (*transition)(int*,int*));

/**
 * Calcule la generation suivante d'un automate cellulaire
 * @param a automate sur lequel passer a la generation suivante
 */
void generation_suivante(ptr_ac a);

/**
 * Affiche un automate cellulaire
 * @param a automate a afficher
 * @param afficher_cellules fonction d'affichage de tableau qui prend en paramètre dans l'ordre : le tableau a afficher
 * (ici une ligne de cellules), la taille de ce tableau, le numéro de la génération des cellules
 */
void afficher_automate_cellulaire(const ptr_ac a,void (*afficher_cellules)(int*, unsigned int, unsigned int));

/**
 * Detruit un automate cellulaire
 * @param a automate a detruire
 */
void detruire_automate_cellulaire(ptr_ac a);

#endif

#include "automate_cellulaire.h"
#include <stdio.h>

void afficher_cellules_4_etats(int* tab, unsigned int taille, unsigned int iteration){
    printf("|%d\t|", iteration);
    for(unsigned int i = 0; i < taille; i++){
        switch (tab[i]){
            case 0:
                printf(" ");
                break;
            case 1:
                printf("X");
                break;
            case 2:
                printf("Y");
                break;
            case 3:
                printf("Z");
                break;
        }
    }
}

int transition_somme(int* voisnage, int* regle){
    return regle[voisnage[0]+voisnage[1]+voisnage[2]];
}

void recuperation_donnee(FILE** pFile, int** pTab, int* pCpt){
    FILE* file = *pFile;
    int cpt = 0;
    int caractereActuel;
    while ((caractereActuel = fgetc(file)) != '\n' && caractereActuel != ' ' && caractereActuel != '#' && caractereActuel != EOF ){
        cpt++;
    }
    int* tab = (int*)malloc(cpt* sizeof(int));
    if (caractereActuel == EOF) fseek(file,-cpt,SEEK_CUR);
    else fseek(file,-cpt-1,SEEK_CUR);

    for (int i = 0; i < cpt; ++i) {
        caractereActuel = fgetc(file);
        tab[i] = caractereActuel-'0';
    }
    *pCpt=cpt;
    *pTab=tab;
}

ptr_ac lire_fichier(char* nom_fichier){

    int* _config, *_regle;
    int _cptConfig, _cptRegle;

    FILE* file = NULL;
    int caractereActuel;
    file = fopen(nom_fichier,"r");
    if (file == NULL){
        printf("Impossible d'ouvrir le fichier config_automate.ac");
        return NULL;
    }


    while ((caractereActuel= fgetc(file)) != EOF){

        if(caractereActuel == '#') {
            while ((caractereActuel = fgetc(file)) != '\n'){
                if(caractereActuel == EOF) break;
            }
            continue;
        }

        if(caractereActuel == 'c'){
            fgetc(file); // Lecture de l'espace
            recuperation_donnee(&file,&_config,&_cptConfig);
        }
        if(caractereActuel == 'r'){
            fgetc(file); // Lecture de l'espace
            recuperation_donnee(&file,&_regle,&_cptRegle);
        }
    }; // On continue tant que fgetc n'a pas retourné EOF (fin de fichier)

    fclose(file);

    ptr_ac ac = creer_automate_cellulaire(4,_config,_cptConfig,_regle,_cptRegle,&transition_somme);
    free(_config);
    free(_regle);
    return ac;
}

int main(int argc, char const *argv[])
{

    int ite = 16;

    ptr_ac a = lire_fichier("config_automate.ac");

    for(int i = 0; i < ite; i++){
        afficher_automate_cellulaire(a,&afficher_cellules_4_etats);
        printf("\n");
        generation_suivante(a);
    }
    detruire_automate_cellulaire(a);
    return 0;
}